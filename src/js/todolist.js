'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

const TodoBanner = () => (
   <h3>TODO ...react.js</h3>
);

const TodoList = ({items}) => (
    <ul>{ items.map(itemText => {
            return <TodoListItem>{ itemText }</TodoListItem>
        })
    }</ul>
);

const TodoListItem = (props) => (
  <li>{ props.children }</li>
)

var TodoForm = React.createClass({
    getInitialState: function() {
        return { item: '' };
    },
    handleSubmit: function(e) {
        e.preventDefault();
        this.props.onFormSubmit(this.state.item);
        this.setState({item: ''});
        React.findDOMNode(this.refs.item).focus();
        return;
    },
    onChange: function(e) {
        this.setState({
            item: e.target.value
        });
    },
    render: function(){
        return (
            <form onSubmit={ this.handleSubmit }>
                <input type='text' ref='item' onChange={ this.onChange } value={ this.state.item } />
                <input type='submit' value='Add'/>
            </form>
        );
    }
});

var TodoApp = React.createClass({
    getInitialState: function() {
        return {items: ['Todo item #1', 'Todo item #2']};
    },
    updateItems: function(newItem) {
        var allItems = this.state.items.concat([newItem]);
        this.setState({
            items: allItems
        });
    },
    render: function() {
        return (
            <div>
                <TodoBanner/>
                <TodoList items={ this.state.items }/>
                <TodoForm onFormSubmit={ this.updateItems } />
            </div>
        );
    }
});

export default TodoApp
