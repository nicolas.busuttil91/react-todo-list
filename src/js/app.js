'use strict';
import React from 'react';
import ReactDOM from 'react-dom';

import Menu from './menu';
import Footer from './footer';
import TodoApp from './todolist';

const App = () => {
    return (
        <div>
            <Menu />
            <TodoApp />
            <Footer />
        </ div>
    )
}

ReactDOM.render(<App/>, document.getElementById('root'));
