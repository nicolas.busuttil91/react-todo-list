'use strict';
import React from 'react';
import ReactDOM from 'react-dom';

const Menu = () => (
    <nav className="navbar navbar-default">
        <div className="container-fluid">
            <div className="navbar-header">
                <a className="navbar-brand" href="#">
                    <img src="img/superman.png" />
                </a>
            </div>
        </div>
    </nav>
)

export default Menu
