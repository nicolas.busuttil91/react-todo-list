var gulp = require('gulp'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin'),
    notify = require("gulp-notify")
    browserify = require('browserify'),
    babelify = require('babelify'),
    fs = require("fs"),
    source = require('vinyl-source-stream'),
    path = require('path')
;

var directories = {
    src: __dirname + '/src',
    public: __dirname + '/public',
    modules: __dirname + '/node_modules'
};

var files = {
    html: [
        directories.src + '/index.html'
    ],

    js: [
        directories.src + '/js/app.js'
    ],

    less: [
        directories.src + '/less/main.less'
    ],

    images: [
        directories.src + '/images/**'
    ]
};

gulp.task('html', function() {
    gulp.src(files.html)
        .pipe(gulp.dest(directories.public))
        .pipe(notify('Html done'));
});

gulp.task('less', function () {
  return gulp.src(files.less)
    .pipe(less({ paths: [path.join(__dirname, 'less', 'includes')] }))
    .pipe(cssmin())
    .pipe(gulp.dest(directories.public + '/css'))
    .pipe(notify('less done'));
});

gulp.task('js', function() {
    browserify(files.js, { debug: true })
        .transform(babelify)
        .bundle()
        .on('error', function(err) { console.log('Error: ' + err.message); })
        .pipe(source('app.js'))
        .pipe(gulp.dest(directories.public + '/js'))
        .pipe(notify('Javascript done'));
});

gulp.task('img', function() {
    gulp.src(files.images)
        .pipe(gulp.dest(directories.public + '/img'))
});

gulp.task('build', ['html', 'less', 'js', 'img']);
gulp.task('default', ['build']);
gulp.task('dev', ['build', 'watch']);

gulp.task('watch', function() {
    gulp.watch(files.html, ['html']);
    gulp.watch(files.less.concat(directories.src + '/less/**.less'), ['less']);
    gulp.watch(files.js, ['js']);

    gulp.watch(__dirname + '/gulpfile.js', ['build']);
});
